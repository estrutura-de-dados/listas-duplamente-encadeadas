

# Classe nó de uma lista vinculada
class DoubleNode:
    # Construtor para criar um novo nó
    def __init__(self, data):
        self.data = data
        self.next = None  # Referência ao próximo nó
        self.previous = None  # referência ao nó anterior


class DoublyLinkedList:
    # Construtor para lista vinculada vazia
    def __init__(self):
        self.head = None
        self.tail = None

    # Dada uma referência à cabeça
    # anexa um novo nó no final
    def append(self, data):
        # Aloca nó e coloca nos dados
        new_node = DoubleNode(data)

        # Este novo nó será o último nó
        new_node.next = None

        # Se a lista vinculada estiver vazia,
        # Faça o novo nó como cabeça
        if self.head == None:
            new_node.previous = None
            self.head = new_node
            return

        last_node = self.head
        while last_node.next:
            last_node = last_node.next

        last_node.next = new_node

        new_node.previous = last_node
        return

    # Retorna o comprimento da lista vinculada.
    def length(self):
        if self.head == None:
            return 0
        current_node = self.head
        total = 0  # Init count
        # Loop enquanto o fim da lista vinculada não é atingido
        while current_node:
            total += 1
            current_node = current_node.next
        return total

    # Converte uma lista vinculada de volta em uma lista Python
    def to_list(self):

        # Init como nulo
        node_data = []
        current_node = self.head

        while current_node:
            node_data.append(current_node.data)
            current_node = current_node.next
        return node_data

    def display(self):
        contents = self.head
        # Se a lista for nula
        if contents is None:
            print("List has no element")
        while contents:
            print(contents.data)
            contents = contents.next
        print("----------")

    # Insere um nó no início
    def insert_at_start(self, data):
        if self.head == None:
            new_node = DoubleNode(data)
            self.head = new_node
            print("Nó inserido")
            return
        new_node = DoubleNode(data)
        new_node.next = self.head
        self.head.previous = new_node
        self.head = new_node

    # Isso insere um nó no final
    def insert_at_end(self, data):
        if self.head == None:
            new_node = DoubleNode(data)
            self.head = new_node
            return
        current_node = self.head
        while current_node.next != None:
            current_node = current_node.next
        new_node = DoubleNode(data)
        current_node.next = new_node
        new_node.previous = current_node

    # Excluindo elementos do início
    def remove_at_start(self):
        if self.head == None:
            print("A lista não tem nenhum elemento para excluir")
            return
        if self.head.next == None:
            self.head = None
            return
        self.head = self.head.next
        self.start_prev = None

    #  Excluindo elementos do final
    def remove_at_end(self):
        if self.head == None:
            print("A lista não tem nenhum elemento para excluir")
            return
        if self.head.next == None:
            self.head = None
            return
        current_node = self.head
        while current_node.next != None:
            current_node = current_node.next
        current_node.previous.next = None

    # Isso remove um nó com o valor especificado
    def remove_element_by_value(self, value):
        if self.head == None:
            print("A lista não tem nenhum elemento para excluir")
            return
        if self.head.next == None:
            if self.head.item == value:
                self.head = None
            else:
                print("Item não encontrado")
            return

        if self.head.data == value:
            self.head = self.head.next
            self.head.previous = None
            return

        current_node = self.head
        while current_node.next != None:
            if current_node.data == value:
                break
            current_node = current_node.next
        if current_node.next != None:
            current_node.previous.next = current_node.next
            current_node.next.previous = current_node.previous
        else:
            if current_node.data == value:
                current_node.previous.next = None
            else:
                print("Elemento não encontrado")


# Test
my_list = DoublyLinkedList()
my_list.display()
# Add the elements
my_list.append(3)
my_list.append(2)
my_list.append(7)
my_list.append(1)

my_list.display()

print("O número total de elementos são: " + str(my_list.length()))
print(my_list.to_list())  # Lista Python
print("---------")

my_list.display()

my_list.remove_at_start()
my_list.display()

my_list.remove_at_end()
my_list.display()

my_list.insert_at_start(1)
my_list.display()

my_list.insert_at_end(3)
my_list.display()

my_list.remove_element_by_value(7)
my_list.display()